#!/bin/bash
if [ -z "$COLOR" ]
then
  COLOR="rgb($(($RANDOM % 255)),$(($RANDOM % 255)),$(($RANDOM % 255)))"
fi
sed -i "s/COLOR/$(echo ${COLOR})/g" /usr/share/nginx/html/index.css
exec "$@"
