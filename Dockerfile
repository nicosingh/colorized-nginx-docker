FROM nginx
COPY index.html /usr/share/nginx/html
COPY index.css /usr/share/nginx/html
COPY entrypoint.sh /usr/local/bin
ENTRYPOINT ["/usr/local/bin/entrypoint.sh"]
CMD ["nginx", "-g", "daemon off;"]
